# Description

This kernel module (DKMS) is from the old legacy 'fglrx-dkms' package ported up to
4.17 Linux kernel version.

# Disclaimer
This kmod is not belongs to me, I just ported it. Everythings that's happened to
you is YOUR OWN RISK. I AM NOT RESPONSIBILITY ABOUT WHAT HAPPENED ON YOUR
MACHINE.

# Requirements
1. Xorg version 1.17
2. Linux kernel 4.17 above

# Changelogs
17/02/2019 ported fglrx-legacy-lts version 15.302

# Installation
1. Clone or download this repo, if you download it, you must extract first.
2. Add it to DKMS as below

   <code>sudo dkms add ./fglrx-legacy-lts-((version))/</code>
3. Install it using DKMS as below

   <code>sudo dkms install fglrx-legacy-lts/((version)) --force</code>
4. Reboot or restart your machine to check are your Radeon is functioned.

# Uninstall
1. Uninstall it using DKMS as below

   <code>sudo dkms remove fglrx-legacy-lts/((version)) --all</code>
4. Reboot or restart your machine.
 
